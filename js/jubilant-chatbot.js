var accessToken = '86a9ccd74c3b4eb6bba8f61dc9ded82e';
var baseUrl = 'https://api.api.ai/v1/';
var recognition;

$(document).ready(function() {

  $(document).on('click', '.question', function() {
    $('#input').val($(this).text());
    send();
  });

  // if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
  //   $('#close-div').css('display', 'none');
  // }

  $('#input').keyup(function(event) {
    var text = $('#input').val();
    changeSendIcon(text);

    if (event.which == 13) {
      event.preventDefault();
      if (!text.replace(/^\s+|\s+$/g, "").length) {
        $('.send-div img').attr('src', 'img/send-50x50.png');
      } else {
        $('.send-div img').attr('src', 'img/B_50x50.png');
        send();
      }
    } else if (event.which == 8 || event.which == 46) {
      changeSendIcon(text);
    }
  });

  $('.send-div').click(function() {
    var text = $('#input').val();
    if (!text.length) {} else {
      send();
    }
  });

  $('.bot-icon-div').click(function() {
    if ($('.bot-icon-div').attr('class') == 'bot-icon-div') {

      $('#chat-bot-div').css('display', 'block');
      $('#chat-head').css('display', 'block');
      $('.bot-img-main').attr('src', 'img/C_100x100.png');
      $('.bot-icon-div').addClass('open');
      $('.input-text').focus();
    } else {

      $('#chat-bot-div').css('display', 'none');
      $('#chat-head').css('display', 'none');
      $('.bot-img-main').attr('src', 'img/150x150-chat.png');
      $('.bot-icon-div').removeClass('open');
    }

  });

});

function startRecognition() {
  recognition = new webkitSpeechRecognition();
  recognition.onstart = function(event) {
    updateRec();
  };
  recognition.onresult = function(event) {
    var text = '';
    for (var i = event.resultIndex; i < event.results.length; ++i) {
      text += event.results[i][0].transcript;
    }
    setInput(text);
    stopRecognition();
  };
  recognition.onend = function() {
    stopRecognition();
  };
  recognition.lang = 'en-US';
  recognition.start();
}

function stopRecognition() {
  if (recognition) {
    recognition.stop();
    recognition = null;
  }
  updateRec();
}

function switchRecognition() {
  if (recognition) {
    stopRecognition();
  } else {
    startRecognition();
  }
}

function setInput(text) {
  $('#input').val(text);
  send();
}

function updateRec() {}

function send() {
  var text = $('#input').val();
  if (text.length) {
    createDiv(text, 'chat-request', 'chat-req-span');
  }
  $('.input-text').val('');
  $('.input-text').prop('disabled', true);
  $.ajax({
    type: 'POST',
    url: baseUrl + 'query?v=20150910',
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    headers: {
      'Authorization': 'Bearer ' + accessToken
    },
    data: JSON.stringify({
      query: text,
      lang: 'en',
      sessionId: '<any random string>'
    }),

    success: function(data) {
      var responseData = data.result.fulfillment.speech;
      setResponse(JSON.stringify(responseData, undefined, 2));
      $('.input-text').prop('disabled', false);
      $('.send-div img').attr('src', 'img/send-50x50.png');
      $('.input-text').focus();
    },
    error: function() {
      $('.input-text').prop('disabled', false);
      $('.send-div img').attr('src', 'img/send-50x50.png');
      $('.input-text').focus();
      //setResponse('Internal Server Error');
    }
  });
  // createDiv('<img class="thinking-img" src="img/bot-thinking.gif" alt="Thinking" width="35" />', 'chat-response', 'chat-res-span');
  // $('.chat-res-span').addClass('chat-thinking-span');


  // setResponse('Thinking...'); // use if want to show Thinking.. before displaying the responce data.
}

function setResponse(val) {
  var height;
  val = val.replace(/\"/g, '');
  if (val.length) {
    createDiv(val, 'chat-response', 'chat-res-span');
  }
  // else {
  //   $('.chat-res-span').removeClass('chat-thinking-span');
  //   $('.chat-div .chat-response:last .chat-res-span').html(val);
  //   $(".chat-div .chat-response:last .chat-res-span i").attr('class', 'question');
  //   $(".chat-div .chat-response:last .chat-res-span i").attr('data-content', 'question');
  //   height = $('.chat-div')[0].scrollHeight;
  //   $('.chat-div').scrollTop(height);
  // }
}


function createDiv(val, divClassName, spanClassName) {
  var iDiv = document.createElement('div');
  iDiv.className = divClassName;
  var innerSpan = document.createElement('span');
  var innerLabel = document.createElement('label');
  innerSpan.className = spanClassName;
  iDiv.appendChild(innerSpan);
  if (divClassName == 'chat-response') {
    innerLabel.className = 'chat-res-label';
    iDiv.appendChild(innerLabel);
  }
  if (divClassName == 'chat-request') {
    innerLabel.className = 'chat-req-label';
    iDiv.appendChild(innerLabel);
  }
  document.getElementsByClassName('chat-div')[0].appendChild(iDiv);
  $('.chat-div .' + divClassName + ':last .' + spanClassName).html(val);
  height = $('.chat-div')[0].scrollHeight;
  $('.chat-div').scrollTop(height);
}

function changeSendIcon(val) {
  if (!$('#input').val().replace(/^\s+|\s+$/g, "").length) {
    $('.send-div img').attr('src', 'img/send-50x50.png');
  } else {
    $('.send-div img').attr('src', 'img/B_50x50.png');
  }
}
